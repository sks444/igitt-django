"""
This module contains the tests for storing GitHub classes of IGitt as
igitt_django models.
"""
from os import environ

from django.test import TransactionTestCase
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubComment import GitHubComment
from IGitt.GitHub.GitHubCommit import GitHubCommit
from IGitt.GitHub.GitHubIssue import GitHubIssue
from IGitt.GitHub.GitHubMergeRequest import GitHubMergeRequest
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.Interfaces.Comment import CommentType
import vcr

from igitt_django.models import IGittComment
from igitt_django.models import IGittCommit
from igitt_django.models import IGittIssue
from igitt_django.models import IGittMergeRequest
from igitt_django.models import IGittRepository

RECORDER = vcr.VCR(match_on=['method', 'scheme', 'host', 'port', 'path'],
                   filter_query_parameters=['access_token'],
                   filter_post_data_parameters=['access_token'])


class GitHubStorageTestCase(TransactionTestCase):
    def setUp(self):
        self.token = GitHubToken(environ.get('GITHUB_TEST_TOKEN', ''))

    @RECORDER.use_cassette('tests/cassettes/github/test_repo.yaml')
    def test_repo(self):
        repo = GitHubRepository(self.token, 'gitmate-test-user/test')
        irepo = IGittRepository.from_igitt_instance(repo)
        cached_repo = irepo.to_igitt_instance(self.token)

        self.assertEqual(cached_repo.full_name, repo.full_name)
        self.assertEqual(cached_repo.hoster, repo.hoster)
        self.assertEqual(cached_repo.data, repo.data)

    @RECORDER.use_cassette('tests/cassettes/github/test_comment.yaml')
    def test_comment(self):
        comment = GitHubComment(self.token, 'gitmate-test-user/test',
                                CommentType.ISSUE, 309221241)
        icomment = IGittComment.from_igitt_instance(comment)
        cached_comment = icomment.to_igitt_instance(self.token)

        self.assertEqual(cached_comment.repository.full_name,
                         comment.repository.full_name)
        self.assertEqual(cached_comment.number, comment.number)
        self.assertEqual(cached_comment.type, comment.type)
        self.assertEqual(cached_comment.data, comment.data)

    @RECORDER.use_cassette('tests/cassettes/github/test_issue.yaml')
    def test_issue(self):
        issue = GitHubIssue(self.token, 'gitmate-test-user/test', 39)
        iissue = IGittIssue.from_igitt_instance(issue)
        cached_issue = iissue.to_igitt_instance(self.token)

        self.assertEqual(cached_issue.number, issue.number)
        self.assertEqual(cached_issue.repository.full_name,
                         issue.repository.full_name)
        self.assertEqual(cached_issue.data, issue.data)

    @RECORDER.use_cassette('tests/cassettes/github/test_commit.yaml')
    def test_commit(self):
        commit = GitHubCommit(self.token, 'gitmate-test-user/test',
                              '645961c0841a84c1dd2a58535aa70ad45be48c46')
        icommit = IGittCommit.from_igitt_instance(commit)
        cached_commit = icommit.to_igitt_instance(self.token)

        self.assertEqual(cached_commit.sha, commit.sha)
        self.assertEqual(cached_commit.repository.full_name,
                         commit.repository.full_name)
        self.assertEqual(cached_commit.data, commit.data)

    @RECORDER.use_cassette('tests/cassettes/github/test_mr.yaml')
    def test_mr(self):
        mr = GitHubMergeRequest(self.token, 'gitmate-test-user/test', 7)
        imr = IGittMergeRequest.from_igitt_instance(mr)
        cached_mr = imr.to_igitt_instance(self.token)

        self.assertEqual(cached_mr.repository.full_name,
                         mr.repository.full_name)
        self.assertEqual(cached_mr.data, mr.data)
