"""
This module contains the tests for storing GitLab classes of IGitt as
igitt_django models.
"""
from os import environ

from django.test import TransactionTestCase
from IGitt.GitLab import GitLabOAuthToken
from IGitt.GitLab.GitLabComment import GitLabComment
from IGitt.GitLab.GitLabCommit import GitLabCommit
from IGitt.GitLab.GitLabIssue import GitLabIssue
from IGitt.GitLab.GitLabMergeRequest import GitLabMergeRequest
from IGitt.GitLab.GitLabRepository import GitLabRepository
from IGitt.Interfaces.Comment import CommentType
import vcr

from igitt_django.models import IGittComment
from igitt_django.models import IGittCommit
from igitt_django.models import IGittIssue
from igitt_django.models import IGittMergeRequest
from igitt_django.models import IGittRepository

RECORDER = vcr.VCR(match_on=['method', 'scheme', 'host', 'port', 'path'],
                   filter_query_parameters=['access_token'],
                   filter_post_data_parameters=['access_token'])


class GitLabStorageTestCase(TransactionTestCase):
    def setUp(self):
        self.token = GitLabOAuthToken(environ.get('GITLAB_TEST_TOKEN', ''))

    @RECORDER.use_cassette('tests/cassettes/gitlab/test_repo.yaml')
    def test_repo(self):
        repo = GitLabRepository(self.token, 'gitmate-test-user/test')
        irepo = IGittRepository.from_igitt_instance(repo)
        cached_repo = irepo.to_igitt_instance(self.token)

        self.assertEqual(cached_repo.full_name, repo.full_name)
        self.assertEqual(cached_repo.hoster, repo.hoster)
        self.assertEqual(cached_repo.data, repo.data)

    @RECORDER.use_cassette('tests/cassettes/gitlab/test_comment.yaml')
    def test_comment(self):
        comment = GitLabComment(self.token, 'gitmate-test-user/test', 1,
                                CommentType.ISSUE, 31500135)
        icomment = IGittComment.from_igitt_instance(comment)
        cached_comment = icomment.to_igitt_instance(self.token)

        self.assertEqual(cached_comment.repository.full_name,
                         comment.repository.full_name)
        self.assertEqual(cached_comment.number, comment.number)
        self.assertEqual(cached_comment.type, comment.type)
        self.assertEqual(cached_comment.data, comment.data)

    @RECORDER.use_cassette('tests/cassettes/gitlab/test_issue.yaml')
    def test_issue(self):
        issue = GitLabIssue(self.token, 'gitmate-test-user/test', 3)
        iissue = IGittIssue.from_igitt_instance(issue)
        cached_issue = iissue.to_igitt_instance(self.token)

        self.assertEqual(cached_issue.number, issue.number)
        self.assertEqual(cached_issue.repository.full_name,
                         issue.repository.full_name)
        self.assertEqual(cached_issue.data, issue.data)

    @RECORDER.use_cassette('tests/cassettes/gitlab/test_commit.yaml')
    def test_commit(self):
        commit = GitLabCommit(self.token, 'gitmate-test-user/test',
                              '3fc4b860e0a2c17819934d678decacd914271e5c')
        icommit = IGittCommit.from_igitt_instance(commit)
        cached_commit = icommit.to_igitt_instance(self.token)

        self.assertEqual(cached_commit.sha, commit.sha)
        self.assertEqual(cached_commit.repository.full_name,
                         commit.repository.full_name)
        self.assertEqual(cached_commit.data, commit.data)

    @RECORDER.use_cassette('tests/cassettes/gitlab/test_mr.yaml')
    def test_mr(self):
        mr = GitLabMergeRequest(self.token, 'gitmate-test-user/test', 7)
        imr = IGittMergeRequest.from_igitt_instance(mr)
        cached_mr = imr.to_igitt_instance(self.token)

        self.assertEqual(cached_mr.repository.full_name,
                         mr.repository.full_name)
        self.assertEqual(cached_mr.data, mr.data)
