"""
This module contains the tests for igitt_django.storage module.
"""
from os import environ

from django.test import TransactionTestCase
from IGitt.GitHub import GitHubToken
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.GitLab.GitLabRepository import GitLabRepository
import pytest
import vcr

from igitt_django.models import IGittRepository
from igitt_django.storage import get_object_or_create

RECORDER = vcr.VCR(match_on=['method', 'scheme', 'host', 'port', 'path'],
                   filter_query_parameters=['access_token'],
                   filter_post_data_parameters=['access_token'])


class IGittDjangoStorageTestCase(TransactionTestCase):
    @RECORDER.use_cassette('tests/cassettes/storage/setup.yaml')
    def setUp(self):
        self.token = GitHubToken(environ.get('GITHUB_TEST_TOKEN', ''))
        self.gh_repo = GitHubRepository(self.token, 'gitmate-test-user/test')
        self.gl_repo = GitLabRepository(self.token, 'gitmate-test-user/test')

        # grab data beforehand to avoid requests again
        self.gh_repo.refresh()
        self.gl_repo.refresh()

        # creates an IGittRepository object and returns it
        self.created_repo_gh = get_object_or_create(
            IGittRepository,
            self.token,
            hoster='github',
            full_name='gitmate-test-user/test')[0].to_igitt_instance(
                self.token)
        self.created_repo_gl = get_object_or_create(
            IGittRepository,
            self.token,
            hoster='gitlab',
            full_name='gitmate-test-user/test')[0].to_igitt_instance(
                self.token)

    def test_created_instance_attributes(self):
        self.assertEqual(self.created_repo_gh.full_name,
                         self.gh_repo.full_name)
        self.assertEqual(self.created_repo_gh.hoster, self.gh_repo.hoster)
        self.assertEqual(self.created_repo_gh.data, self.gh_repo.data)

        self.assertEqual(self.created_repo_gl.full_name,
                         self.gl_repo.full_name)
        self.assertEqual(self.created_repo_gl.hoster, self.gl_repo.hoster)
        self.assertEqual(self.created_repo_gl.data, self.gl_repo.data)

    def test_retrieve_instance(self):
        # retrieves the stored IGittRepository object
        retrieved_repo_gh, _ = get_object_or_create(
            IGittRepository,
            self.token,
            hoster='github',
            full_name='gitmate-test-user/test')
        retrieved_repo_gl, _ = get_object_or_create(
            IGittRepository,
            self.token,
            hoster='gitlab',
            full_name='gitmate-test-user/test')

        self.assertEqual(retrieved_repo_gh.data, self.gh_repo.data)
        self.assertEqual(retrieved_repo_gl.data, self.gl_repo.data)

    def test_non_model_get_object(self):
        with pytest.raises(ValueError):

            class SomeRandomClass:
                pass

            # calling get_object_or_create() on some class which is neither
            # a Model nor a QuerySet nor a Manager, raises a ValueError
            # exception.
            get_object_or_create(SomeRandomClass, self.token)
