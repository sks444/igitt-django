#!/usr/bin/env python3
"""
Provides a main routine that can install igitt_django and create distributions.
"""
from os.path import join, dirname

from setuptools import setup

from igitt_django import VERSION


def load_requirements():
    return open(join(dirname(__file__), 'requirements.txt'), 'r').readlines()


if __name__ == '__main__':
    setup(
        name='igitt_django',
        version=VERSION,
        description='A django app for storing IGitt objects.',
        author='Naveen Kumar Sangi',
        maintainer='Naveen Kumar Sangi',
        maintainer_email='nkprince007@gmail.com',
        packages=[
            'igitt_django',
            'igitt_django.migrations',
        ],
        install_requires=load_requirements(),
        package_data={'igitt_django': ['VERSION']},
        license='MIT')
