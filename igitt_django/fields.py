"""
This module contains the additional fields used by igitt_django.
"""
from enum import Enum

from django.db.models import CharField


class EnumField(CharField):
    """
    A wrapper field for CharField to use python enumerated structures.
    """

    def __init__(self, enum: Enum, *args, **kwargs):
        self.enum = enum
        longest_enum_value = max(len(item.value) for item in enum)
        choices = [(item.name, item.value) for item in enum]
        super(EnumField, self).__init__(
            *args, **kwargs, max_length=longest_enum_value, choices=choices)

    def get_prep_value(self, value):  # dont cover
        if value is None:
            return None
        if isinstance(value, self.enum):
            return value.value
        return self.enum(value).value

    def deconstruct(self):
        name, path, args, kwargs = super(EnumField, self).deconstruct()
        args.insert(0, self.enum)
        del kwargs['choices']
        del kwargs['max_length']
        return name, path, args, kwargs
