"""
This module contains helpers for fetching and updating IGitt objects.
"""
from IGitt.Interfaces import Token


def _get_queryset(klass):
    """
    Returns a QuerySet or a Manager for the class.
    Duck typing in action; any class with a `get()` method might do the job.
    """
    # If it is a model class or anything else with ._default_manager
    if hasattr(klass, '_default_manager'):
        return klass._default_manager.all()
    return klass  # dont cover


def get_object_or_create(klass, token: Token=None, **kwargs):
    """
    Retrieves an object if it exists, or creates a new one and stores it in the
    database and returns it.

    :param klass:
        klass may be a Model, Manager, or QuerySet object. All other passed
        arguments and keyword arguments are used in the `get()` query.
    :param token:
        The access token to be used to fetch data from supported hosting
        services.
    :return:
        A tuple of (object, created), where object is the retrieved or created
        object and created is a boolean specifying whether a new object was
        created.
    """
    queryset = _get_queryset(klass)
    try:
        return queryset.get(**kwargs), False
    except AttributeError:
        klass__name = (klass.__name__ if isinstance(klass, type) else
                       klass.__class__.__name__)
        raise ValueError(
            'First argument to get_object_or_create() must be a Model, '
            "Manager, or QuerySet, not '%s'." % klass__name)
    except queryset.model.DoesNotExist:
        model_fields = [field.name for field in queryset.model._meta.fields]
        req_kwargs = {k: v for k, v in kwargs.items() if k in model_fields}
        model_object = queryset.create(**req_kwargs)
        igitt_instance = model_object.to_igitt_instance(token)
        igitt_instance.refresh()
        model_object.data = igitt_instance.data
        model_object.save()
        return model_object, True
