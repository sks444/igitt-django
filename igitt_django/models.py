"""
This module contains all the IGitt compatible django models.
"""
from django.contrib.postgres import fields as psql_fields
from django.db import models
from IGitt.GitHub.GitHubComment import GitHubComment
from IGitt.GitHub.GitHubCommit import GitHubCommit
from IGitt.GitHub.GitHubIssue import GitHubIssue
from IGitt.GitHub.GitHubMergeRequest import GitHubMergeRequest
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.GitLab.GitLabComment import GitLabComment
from IGitt.GitLab.GitLabCommit import GitLabCommit
from IGitt.GitLab.GitLabIssue import GitLabIssue
from IGitt.GitLab.GitLabMergeRequest import GitLabMergeRequest
from IGitt.GitLab.GitLabRepository import GitLabRepository
from IGitt.Interfaces import Token
from IGitt.Interfaces.Comment import Comment
from IGitt.Interfaces.Comment import CommentType
from IGitt.Interfaces.Commit import Commit
from IGitt.Interfaces.Issue import Issue
from IGitt.Interfaces.MergeRequest import MergeRequest
from IGitt.Interfaces.Repository import Repository

from igitt_django.fields import EnumField


class IGittBase(models.Model):
    """
    The abstract base class for all IGitt models.
    """
    data = psql_fields.JSONField(default={})

    @classmethod
    def from_igitt_instance(cls, instance):
        """
        Creates a model instance from IGitt instance.
        """
        raise NotImplementedError

    @property
    def igitt_class(self):
        """
        Returns the corresponding class the model relates to.
        """
        raise NotImplementedError

    @property
    def init_args(self):
        """
        Retrieves a dict of args required for creating IGitt instance, with
        keys as model fields mapped to corresponding arguments.
        """
        raise NotImplementedError

    def to_igitt_instance(self, token: Token):
        """
        Creates an IGitt instance from stored model data.
        """
        return self.igitt_class.from_data(self.data, token, **self.init_args)

    def __str__(self):
        raise NotImplementedError

    class Meta:
        abstract = True


class IGittRepository(IGittBase):
    """
    A model based on IGitt's Repository interface.
    """
    full_name = models.CharField(default=None, max_length=255)
    hoster = models.CharField(default=None, max_length=32)

    @classmethod
    def from_igitt_instance(cls, instance: Repository):
        """
        Creates an IGittRepository model instance from Repository object.
        """
        return cls.objects.get_or_create(
            full_name=instance.full_name,
            hoster=instance.hoster,
            defaults={'data': instance.data})[0]

    @property
    def init_args(self):
        """
        Retrieves a dict of args required for creating IGitt's Repository
        object, with keys as model fields mapped to corresponding arguments.
        """
        return {'repository': self.full_name}

    @property
    def igitt_class(self):
        """
        Returns the corresponding class the model relates to.
        """
        if self.hoster.lower() == 'github':
            return GitHubRepository
        elif self.hoster.lower() == 'gitlab':
            return GitLabRepository

        raise NotImplementedError

    def __str__(self):  # dont cover
        return '{}/{}'.format(self.hoster, self.full_name)


class IGittComment(IGittBase):
    """
    A model based on IGitt's Comment interface.
    """
    repo = models.ForeignKey(
        IGittRepository, on_delete=models.CASCADE, related_name='comments')
    iid = models.CharField(default='', max_length=40)
    number = models.IntegerField(default=0)
    type = EnumField(enum=CommentType)

    @classmethod
    def from_igitt_instance(cls, instance: Comment):
        kwargs = {
            'repo': IGittRepository.from_igitt_instance(instance.repository),
            'number': instance.number,
            'type': instance.type
        }
        if hasattr(instance, 'iid'):  # For GitLab
            kwargs['iid'] = instance.iid

        return cls.objects.get_or_create(
            **kwargs, defaults={'data': instance.data})[0]

    @property
    def init_args(self):
        """
        Retrieves a dict of args required for creating IGitt's Repository
        object, with keys as model fields mapped to corresponding arguments.
        """
        kwargs = {
            'comment_id': self.number,
            'repository': self.repo.full_name,
            'comment_type': self.type,
        }
        if hasattr(self.igitt_class, 'iid'):
            kwargs['iid'] = self.iid
        return kwargs

    @property
    def igitt_class(self):
        """
        Returns the corresponding class the model relates to.
        """
        if self.repo.hoster.lower() == 'github':
            return GitHubComment
        elif self.repo.hoster.lower() == 'gitlab':
            return GitLabComment

        raise NotImplementedError

    def __str__(self):  # dont cover
        return '{}:{}@{}'.format('comment', self.repo.full_name, self.number)


class IGittIssue(IGittBase):
    """
    A model based on IGitt's Issue interface.
    """
    repo = models.ForeignKey(
        IGittRepository, on_delete=models.CASCADE, related_name='issues')
    number = models.IntegerField(default=0)

    @classmethod
    def from_igitt_instance(cls, instance: Issue):
        return cls.objects.get_or_create(
            repo=IGittRepository.from_igitt_instance(instance.repository),
            number=instance.number,
            defaults={'data': instance.data})[0]

    @property
    def init_args(self):
        """
        Retrieves a dict of args required for creating IGitt's Repository
        object, with keys as model fields mapped to corresponding arguments.
        """
        return {'number': self.number, 'repository': self.repo.full_name}

    @property
    def igitt_class(self):
        """
        Returns the corresponding class the model relates to.
        """
        if self.repo.hoster.lower() == 'github':
            return GitHubIssue
        elif self.repo.hoster.lower() == 'gitlab':
            return GitLabIssue

        raise NotImplementedError

    def __str__(self):  # dont cover
        return '{}:{}@{}'.format('issue', self.repo.full_name, self.number)


class IGittCommit(IGittBase):
    """
    A model based on IGitt's Commit interface.
    """
    repo = models.ForeignKey(
        IGittRepository, on_delete=models.CASCADE, related_name='commits')
    sha = models.CharField(default=None, max_length=40)

    @classmethod
    def from_igitt_instance(cls, instance: Commit):
        return cls.objects.get_or_create(
            repo=IGittRepository.from_igitt_instance(instance.repository),
            sha=instance.sha,
            defaults={'data': instance.data})[0]

    @property
    def init_args(self):
        """
        Retrieves a dict of args required for creating IGitt's Repository
        object, with keys as model fields mapped to corresponding arguments.
        """
        return {'sha': self.sha, 'repository': self.repo.full_name}

    @property
    def igitt_class(self):
        """
        Returns the corresponding class the model relates to.
        """
        if self.repo.hoster.lower() == 'github':
            return GitHubCommit
        elif self.repo.hoster.lower() == 'gitlab':
            return GitLabCommit

        raise NotImplementedError

    def __str__(self):  # dont cover
        return '{}:{}@{}'.format('commit', self.repo.full_name, self.sha)


class IGittMergeRequest(IGittBase):
    """
    A model based on IGitt's MergeRequest interface.
    """
    repo = models.ForeignKey(
        IGittRepository,
        on_delete=models.CASCADE,
        related_name='merge_request')
    number = models.IntegerField(default=0)

    @classmethod
    def from_igitt_instance(cls, instance: MergeRequest):
        return cls.objects.get_or_create(
            repo=IGittRepository.from_igitt_instance(instance.repository),
            number=instance.number,
            defaults={'data': instance.data})[0]

    @property
    def init_args(self):
        """
        Retrieves a dict of args required for creating IGitt's Repository
        object, with keys as model fields mapped to corresponding arguments.
        """
        return {'number': self.number, 'repository': self.repo.full_name}

    @property
    def igitt_class(self):
        """
        Returns the corresponding class the model relates to.
        """
        if self.repo.hoster.lower() == 'github':
            return GitHubMergeRequest
        elif self.repo.hoster.lower() == 'gitlab':
            return GitLabMergeRequest

        raise NotImplementedError

    def __str__(self):  # dont cover
        return '{}:{}@{}'.format('merge_request', self.repo.full_name,
                                 self.number)
