"""
This module contains the application configuration for igitt_django.
"""
from django.apps import AppConfig


class IgittDjangoConfig(AppConfig):
    name = 'igitt_django'
