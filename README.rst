igitt-django
============

[IGitt](https://gitlab.com/gitmate/open-source/IGitt) is a simple python
library that allows you to access various git hosting
services like GitHub, GitLab and so on via one unified python interface.

Description
-----------

This is the `Django <https://www.djangoproject.com/>`_ component of IGitt that
allows you to store IGitt objects and retrieve them without external requests.
It implements the needed functionality to integrate IGitt in a django based
project.

Setup
-----

.. code-block:: bash

    $ pip3 install igitt-django

License
-------

This project is licensed under **MIT License**.

Maintenance
-----------

igitt-django is maintained by Naveen Kumar Sangi (nkprince007@gmail.com)
